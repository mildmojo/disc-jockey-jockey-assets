var fs = require('fs');
var path = require('path');

var catalog = {
  intros: [],
  ads: []
};

function isFile(filePath) {
  return fs.existsSync(filePath) && fs.statSync(filePath).isFile();
}

function isDir(dirPath) {
  return fs.existsSync(dirPath) && fs.statSync(dirPath).isDirectory();
}

var stations = fs.readdirSync('stations');
stations.forEach(function(station) {
  if (!isDir(path.join('stations', station))) return;

  catalog[station] = {
    songs: {},
    talks: {},
    stationIDs: {},
    preChatters: {},
    postChatters: {},
    backAnnounces: {}
  };

  for (var kind in catalog[station]) {
    kindPath = path.join('stations', station, kind);
    if (!isDir(kindPath)) continue;

    var files = fs.readdirSync(kindPath);
    files.forEach(function(file) {
      if (!isFile(path.join(kindPath, file))) return;

      var parts = file.split('.');
      if (parts.length > 1) parts.pop();
      var key = parts.join();
      catalog[station][kind][key] = path.join(kindPath, file);
    });
  }
});

if (isDir('intros')) {
  catalog.intros = fs.readdirSync('intros').filter(isFile);
}
if (isDir('ads')) {
  catalog.ads = fs.readdirSync('ads').filter(isFile);
}

fs.writeFile('assets.js', 'module.exports = ' + JSON.stringify(catalog) + ';');
